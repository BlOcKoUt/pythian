Assignments for Pythian interview


Assignment 1:
- Using a vagrantfile which executes any configuration management framework (e.g. chef, puppet, ansible or salt) or docker, automatically stand up a data driven website (of any stack) from scratch.

Assignment 2:
- In any language, write a script that:
- Takes an ip address and a list of ports as standard input.  
- Check if each of the ports is active.
- If the port is down, send a command to turn the service on via ssh.
- Write the results to a log.
- extra credit: include a vagrantfile to run the script against

Assignment 3:
- Create a vagrant box that runs hadoop and monitoring tools watching it.

