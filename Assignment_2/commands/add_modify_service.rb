if ARGV.count != 4 || ARGV.first == "-h"
  puts AMS_USAGE
  exit(1)
end

if ARGV.first.match(/^\d{1,5}$/) && ARGV.first.to_i < 65535
  port = ARGV.first
  ARGV.shift
else
  puts AMS_WRONG_PORT
  puts AMS_USAGE
  exit(1)
end

ips = ARGV.first.split(",")
ips.each do |ip|
  if !Checker.valid_ip?(ip)
    puts AMS_WRONG_IP
    puts AMS_USAGE
    exit(1)
  end
end

ARGV.shift
name = ARGV.first
ARGV.shift
service = ARGV.first

allservices = Restart::Services.all

if ams = Restart::Services.find_by_port(port)
  allservices[port].name = name
  allservices[port].ips = ips
  allservices[port].service = service
  Util.log "checker.log", "Modified service #{name}. Port is #{port}, allowed IPs #{ips.inspect} and service command is #{service}."
else
  allservices[port] = Restart::Services.new(name, service, ips)
  Util.log "checker.log", "Added service #{name}. Port is #{port}, allowed IPs #{ips.inspect} and service command is #{service}."
end

File.open('./etc/services.yml', 'w') {|f| YAML.dump(allservices, f) }