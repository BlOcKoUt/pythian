
if (ARGV.count > 3 || ARGV.count < 2) || ARGV.first == "-h"
  puts SS_USAGE
  exit(1)
end

ip = Checker.valid_ip?(ARGV.first)

if ip && ip.count == 1

  ip = ip[0]
  
  ARGV.shift 
  
  port = Checker.valid_port?(ARGV.first)

  if !port
    service = Restart::Services.find_by_name(ARGV.first.downcase)
    port = service[0].to_i if service
  elsif port.count == 1
    port = port[0].to_i
  else
    port = nil
  end

  if port
  
    service = Restart::Services.find_by_port(port.to_s)
    ARGV.shift
    username = nil
    
    ARGV.each do |x|
      username = x[2..-1] if x.start_with?("-u")
    end
    
    if service
      
      if service.ips.include? ip
        
        if username
          
          print "Trying to bring up service #{service.name} (#{port}) on IP #{ip} as user #{username}... "
          Util.log "checker.log", "Trying to bring up service #{service.name} (#{port}) on IP #{ip} as user #{username}. Here are STDOUT and STDERR:"
          
          Restart::Services.now(ip,service.service,username)
          sleep 1
          
          if Checker.port_open?(ip, port)
            puts "[\e[0;32mSUCCESS\e[0m]"
            Util.log "checker.log", "SUCCESS!!!"
          else
            puts "[\e[0;31mFAIL\e[0m]"
            Util.log "checker.log", "We have failed :(", "error"
          end
          
        else
          
          print "Trying to bring up service #{service.name} (#{port}) on IP #{ip}... "
          Util.log "checker.log", "Trying to bring up service #{service.name} (#{port}) on IP #{ip}. Here are STDOUT and STDERR:"
          
          Restart::Services.now(ip,service.service)
          sleep 1
          
          if Checker.port_open?(ip, port)
            puts "[\e[0;32mSUCCESS\e[0m]"
            Util.log "checker.log", "SUCCESS!!!"
          else
            puts "[\e[0;31mFAIL\e[0m]"
            Util.log "checker.log", "We have failed :(", "error"
          end
          
        end
        
      else
        Util.log "checker.log", "On IP #{ip} we have tried to start a service on port #{port} through #{username ? "ssh" : "vagrant"}, but IP is not allowed.", "warn"
        puts IP_NOT_ALLOWED
        puts SS_USAGE
        exit(1)
      end
      
    else
      Util.log "checker.log", "On IP #{ip} we have tried to start a service on port #{port} through #{username ? "ssh" : "vagrant"}, but service is not defined.", "warn"
      puts SERVICE_UNSET
      puts SS_USAGE
      exit(1)
    end

  else
    puts WRONG_PORT
    puts SS_USAGE
    exit(1)
  end
  
else
  puts WRONG_IP
  puts SS_USAGE
  exit(1)
end  