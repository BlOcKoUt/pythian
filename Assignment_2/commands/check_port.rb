if (ARGV.count > 5 || ARGV.count < 2) || ARGV.first == "-h"
  puts CP_USAGE
  exit(1)
end

if check_ips = Checker.valid_ip?(ARGV.first)

  ARGV.shift 

  if check_ports = Checker.valid_port?(ARGV.first)

    ARGV.shift
    restart_services = true
    username = nil
    timeout = 1
    
    ARGV.each do |x|
      restart_services = false if x == "-nr"
      username = x[2..-1] if x.start_with?("-u")
      timeout = x[2..-1].to_f if x.start_with?("-t")
    end
    
    puts "\nChecking port(s) #{check_ports.join(",")} on IPs #{check_ips.join(",")}."
    Util.log "checker.log", "Checking port(s) #{check_ports.join(",")} on IP(s) #{check_ips.join(",")}."
    puts "\nNotice: If port is closed we will \e[0;31mNOT\e[0m try to restart it.\n\n" if !restart_services
    Util.log "checker.log", "If port is closed we will NOT try to restart it."
    puts "\nNotice: We will use #{username ? "SSH with username #{username}" : "Vagrant SSH"} to restart service(s)\n\n" if restart_services
    Util.log "checker.log", "We will use #{username ? "SSH with username #{username}" : "Vagrant SSH"} to restart service(s)"
    
    check_ips.each do |ip|
      check_ports.each do |port|
        
        if Checker.port_open?(ip, port, timeout)
          puts "Port #{port} on IP #{ip} status... [\e[0;32mUP\e[0m]"
          Util.log "checker.log", "Port #{port} on IP #{ip} is UP."
        else
          puts "Port #{port} on IP #{ip} status... [\e[0;31mDOWN\e[0m]"
          Util.log "checker.log", "Port #{port} on IP #{ip} is DOWN.", "warn"
          
          
          if restart_services

            service = Restart::Services.find_by_port(port.to_s)
            
            if service
              
              if service.ips.include? ip
                
                if username
                  
                  print "Trying to bring up service #{service.name} (#{port}) on IP #{ip} as user #{username}... "
                  Util.log "checker.log", "Trying to bring up service #{service.name} (#{port}) on IP #{ip} as user #{username}. Here are STDOUT and STDERR:"
                  
                  Restart::Services.now(ip,service.service,username)
                  sleep 1
                  
                  if Checker.port_open?(ip, port)
                    puts "[\e[0;32mSUCCESS\e[0m]"
                    Util.log "checker.log", "SUCCESS!!!"
                  else
                    puts "[\e[0;31mFAIL\e[0m]"
                    Util.log "checker.log", "We have failed :(", "error"
                  end
                  
                else
                  
                  print "Trying to bring up service #{service.name} (#{port}) on IP #{ip}... "
                  Util.log "checker.log", "Trying to bring up service #{service.name} (#{port}) on IP #{ip}. Here are STDOUT and STDERR:"
                  
                  Restart::Services.now(ip,service.service)
                  sleep 1
                  
                  if Checker.port_open?(ip, port)
                    puts "[\e[0;32mSUCCESS\e[0m]"
                    Util.log "checker.log", "SUCCESS!!!"
                  else
                    puts "[\e[0;31mFAIL\e[0m]"
                    Util.log "checker.log", "We have failed :(", "error"
                  end
                  
                end
                
              else
                Util.log "checker.log", "On IP #{ip} we have tried to start a service on port #{port} through #{username ? "ssh" : "vagrant"}, but IP is not allowed.", "warn"
              end
              
            else
              Util.log "checker.log", "On IP #{ip} we have tried to start a service on port #{port} through #{username ? "ssh" : "vagrant"}, but service is not defined.", "warn"
            end
            
            
          end
        end
        
      end
    end

  else
    puts CP_WRONG_PORT
    puts CP_USAGE
    exit(1)
  end
  
else
  puts CP_WRONG_IP
  puts CP_USAGE
  exit(1)
end  


