if ARGV.first == "-h"
  puts LS_USAGE
  exit(1)
end

Util.log "checker.log", "Services listed"

puts "\nHere is the list of all services and IP addresses enabled for them:\n\n"

services = Restart::Services.all

services.each do |service|
  puts "Name: #{service[1].name}"
  puts "\tPort: #{service[0]}"
  puts "\tService: #{service[1].service}"
  puts "\tAllowed IPs: #{service[1].ips.join(", ")}"
  puts "\n"
end