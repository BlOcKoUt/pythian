require './lib/check_port.rb'
require './lib/services_restart.rb'
require './lib/util.rb'

Restart::Services.load

module Commands

  class CommandsTasks
    attr_reader :argv

    COMMAND_WHITELIST = %w(check_port start_service add_modify_service list_services version help)

    def initialize(argv)
      @argv = argv
    end

    def run_command!(command)
      command = parse_command(command)
      if COMMAND_WHITELIST.include?(command)
        send(command)
      else
        write_error_message(command)
      end
    end

    def check_port
      require_command!("check_port")
    end
    
    def start_service
      require_command!("start_service")
    end
    
    def add_modify_service
      require_command!("add_modify_service")
    end
    
    def list_services
      require_command!("list_services")
    end

    def version
      puts SCRIPT_VERSION
      exit(1)
    end

    def help
      write_help_message
    end

    private

      def require_command!(command)
        require "./commands/#{command}"
      end

      def write_help_message
        puts HELP_MESSAGE
      end

      def write_error_message(command)
        puts "\nError: Command '#{command}' not recognized"
        write_help_message
        exit(1)
      end

      def parse_command(command)
        case command
        when '--version', '-v'
          'version'
        when '--help', '-h'
          'help'
        else
          command
        end
      end
  end
end
