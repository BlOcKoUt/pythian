#!/usr/bin/env ruby

require './etc/messages'

ARGV << '--help' if ARGV.empty?

aliases = {
  "cp" => "check_port",
  "ss" => "start_service",
  "ls" => "list_services",
  "ams" => "add_modify_service",
  "-v" => "version",
  "-h" => "help",
}

command = ARGV.shift.downcase
command = aliases[command] || command

require './commands/commands_tasks'

Commands::CommandsTasks.new(ARGV).run_command!(command)
