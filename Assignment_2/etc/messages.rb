USER = `whoami`.strip!

SCRIPT_VERSION = "Version 1.0 # Pythian interview"

HELP_MESSAGE = <<-EOT

Usage: checker.rb COMMAND [ARGS] [OPTIONS]

Commands are:

 check_port (short-cut alias: "cp")               Chek is port(s) open on selected IP(s).
 start_service (short-cut alias: "ss")            Start service on selected IP.
 list_services (short-cut alias: "ls")            List all defined services and allowed IPs.
 add_modify_service (short-cut alias: "ams")      Add/modify services and allowed IPs.
 version (short-cut alias: "-v")                  Version of script.

All commands can be run with -h for more information.

EXAMPLES:

  ./checker.rb cp 172.16.32.42 21,53,80,3306,6379,11211,27017 -nr -t0.1
  - This will check selected ports on provided vagrant box but will NOT try to restart if down

  ./checker.rb cp 172.16.32.42 21,53,80,3306,6379,11211,27017
  - This will check selected ports on provided vagrant box and try to restart if down
  
  ./checker.rb ls 
  - This will show list of all defined services, ports and IPs for which is restart allowed.
  
  ./checker.rb ams 443 10.0.1.114,192.168.12.6 https apache2
  - This will add/modify apache2 as service for https, set port to 443 and allow services restart
  for IPs 10.0.1.114 and 192.168.12.6
                                                                 
  ./checker.rb ss 172.16.32.1 mysql -upythian 
  - This will try to start service mysql on IP 172.16.32.1 through ssh, as user pythian.
  You should have authorized keys set and sudo enabled.
  
  ./checker.rb ss 172.16.32.1 redis
  - This will try to start service redis on IP 172.16.32.1 through vagrant ssh
  
  ./checker.rb cp 172.16.32.1/24 1-1000 -nr -t0.5
  - This will check ports 1 to 1000 on class C subnet 172.16.32.1/24. Might take a while ;)
  It will NOT try to restart defined services if ports are closed. Timeout is set to 0.5 seconds.
  
  ./checker.rb cp 172.16.32.42 1..100 -upythian
  - This will check ports 1 to 100 on IP 172.16.32.42 and will try to restart if services are 
  defined and IP allowed.

EOT

LS_USAGE = <<-EOT

Usage: checker.rb ls

This will list all defined services, ports of those services and
IP addresses for which service restart is allowed.

EOT

AMS_USAGE = <<-EOT

Usage: checker.rb ams PORT IP(s) NAME SERVICE

This command will modify existing services OR add new one if it's not already defined.
Checker will try to restart service(s) only on IP addresses which are allowed!

PORT - Port on which service is listening
IP(s) - Comma delimited list of IP addresses on which we are allowed to restart service(s)
NAME - Friendly name for service.
SERVICE - Service that we will try to start/stop through service start/stop command on remote box.

EXAMPLE:

  ./checker.rb ams 443 10.0.1.114,192.168.12.6 https apache2
  - This will add/modify apache2 as service for https, set port to 443 and allow services restart
  for IPs 10.0.1.114 and 192.168.12.6

EOT

SS_USAGE = <<-EOT

Usage: checker.rb ss IP PORT/NAME [-uUSERNAME]

This command will try to start a service on IP address given. Service should be previously defined and
IP address should be allowed for that service. You can use port number OR name of service.

By default, script will try to start a service through vagrant ssh. It will try to executing:
vagrant ssh IP -c "sudo service start"
Your vagrant box hostname should be the same as IP address.

If -uUSERNAME is used, script will try to start a service through ssh, using USERNAME. You need to have
authorized_keys set, so we can ssh withot password and sudo should be enabled for that user.

EXAMPLES:

  ./checker.rb ss 172.16.32.1 mysql -upythian 
  - This will try to start service mysql on IP 172.16.32.1 through ssh, as user pythian.

  ./checker.rb ss 10.0.1.2 redis
  - This will try to start service redis on IP 10.0.1.2 through vagrant ssh

EOT

CP_USAGE = <<-EOT

Usage: checker.rb cp IP[/SUBNET] PORT[,/-/..PORT] [-nr] [-uUSERNAME]  [-tTIMEOUT]

This command will check if selected port(s) are open on IP/SUBNET. If port is closed, service defined
and IP allowed for that service, script will try to restart it if -nr option is not used.

OPTIONS:

  IP/SUBNET - Single IP address or CIDR. You can use e.g. 10.1.2.3/24.
  
  PORT - List of ports that should be checked. Ports should be comma delimited or you can use .. or - for range.
  
  -nr - If used, script will not try to restart service(s). It will just check open ports.

  -uUSERNAME - If used, script will try to start a service through ssh, using USERNAME. You need to have
  authorized_keys set, so we can ssh withot password and sudo should be enabled for that user. If not used
  script will try to start a service through vagrant ssh. Your vagrant box hostname should be the same as IP address.
  
  -tTIMEOUT - If used, script will set port open timeout to given time. It should be reasonable float number. Default is 1s.
  
EXAMPLES:

  ./checker.rb cp 172.16.32.1/24 1-1000 -nr -t0.5
  - This will check ports 1 to 1000 on class C subnet 172.16.32.1/24. Might take a while ;)
  It will NOT try to restart defined services if ports are closed. Timeout is set to 0.5 seconds.

  ./checker.rb cp 172.16.32.42 1..100 -upythian
  - This will check ports 1 to 100 on IP 172.16.32.42 and will try to restart if services are 
  defined and IP allowed.

EOT

SERVICE_UNSET = <<-EOT

This service is not set. Please use [ams] script command to setup service.
EOT

IP_NOT_ALLOWED = <<-EOT

This IP address is not allowed for service start. Please use [ams] script command to allow service start on it.
EOT

WRONG_PORT = <<-EOT

You have used wrong port format. Port should be integer lover than 65536.
EOT

WRONG_IP = <<-EOT

You have used wrong IP(s) format.
EOT

CP_WRONG_PORT = <<-EOT

You have used wrong port format. Port should be integer lover than 65536.
You can use comma delimited list of ports e.g. 21,25,53,110,143,443 or you can use
a port range with .. or - e.g. 1-100 or 345..1024
EOT

CP_WRONG_IP = <<-EOT

You have used wrong IP format. You can use single IP e.g. 10.11.12.13 or you can scan whole
subnet. e.g. 192.168.0.1/16
EOT
