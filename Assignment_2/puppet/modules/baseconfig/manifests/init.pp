# == Class: baseconfig
#
# Performs initial configuration tasks for all Vagrant boxes.
#
class baseconfig {
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update';
  }

  host {
    '172.16.32.42':
      ip => '172.16.32.42';
  }

  file {
    '/home/vagrant/.bashrc':
      owner => 'vagrant',
      group => 'vagrant',
      mode  => '0644',
      source => 'puppet:///modules/baseconfig/bashrc';
  }

  file {
    '/etc/redis/redis.conf':
      owner => 'root',
      group => 'root',
      mode  => '0644',
      source => 'puppet:///modules/baseconfig/redis.conf',
			require => Package['redis-server'],
			notify  => Service['redis-server'];
  }

  file {
    '/etc/mongodb.conf':
      owner => 'root',
      group => 'root',
      mode  => '0644',
      source => 'puppet:///modules/baseconfig/mongodb.conf',
			require => Package['mongodb-server'],
			notify  => Service['mongodb'];
  }
	
  file { '/etc/mysql/my.cnf':
    owner => 'root',
    group => 'root',
    mode  => '0644',
    source  => 'puppet:///modules/baseconfig/my.cnf',
    require => Package['mysql-server'],
    notify  => Service['mysql'];
  }
	

  file { '/etc/init.d/memcached':
    owner => 'root',
    group => 'root',
    mode  => '0755',
    source  => 'puppet:///modules/baseconfig/memcached';
  }
	

  package { ['vsftpd']: ensure => present }
	

  package { ['memcached']: ensure  => present,
		require => File['/etc/init.d/memcached'];
  }
	
  package { ['mongodb-server']: ensure => present,
		before => File['/etc/mongodb.conf'];
  }
	
  package { ['mysql-server']: ensure => present,
		require => Exec['apt-get update'];
  }

	package { ['redis-server']: ensure => present,
		before => File['/etc/redis/redis.conf'];
  }
	package { ['bind9']: ensure => present,
    require => Exec['apt-get update'];
  }
	
	package { ['apache2']: ensure => present,
	  require => Exec['apt-get update'];
  }

  service { mongodb: ensure => "running",
		require => File['/etc/mongodb.conf'];
 }

  service { 'memcached': ensure => "running",
	  require => Package['memcached'];
  }
	
  service { 'redis-server': ensure  => running,
    require => File['/etc/redis/redis.conf'];
  }
	
  service { 'mysql': ensure  => running,
    require => Package['mysql-server'];
  }

  service { 'bind9': ensure  => running,
    require => Package['bind9'];
  }

  service { 'apache2': ensure  => running,
    require => Package['apache2'];
  }
		
  service { 'vsftpd': ensure => "running" }

  exec { 'set-mysql-password':
    unless  => 'mysqladmin -uroot -proot status',
    command => "mysqladmin -uroot password root",
    path    => ['/bin', '/usr/bin'],
    require => Service['mysql'];
  }


}
