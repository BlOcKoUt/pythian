Assigment 2:

1) In any language, write a script that:
2) Takes an ip address and a list of ports as standard input.  
3) Check if each of the ports is active.
4) If the port is down, send a command to turn the service on via ssh.
5) Write the results to a log.
6) extra credit: include a vagrantfile to run the script against


Solution:

1) Used ruby
2) done
3) done
4) done
5) done
6) done


Usage:

1) Bring up vagrant box. It will have IP 172.16.32.42
2) Check some ports on the box e.g.
  ./checker.rb cp 172.16.32.42 21,53,80,3306,6379,11211,27017 -nr
  - This will check selected ports but will not try to restart services if down (should me up first time)
3) ssh to vagrant box and stop some services
4) Try to bring them back up
  ./checker.rb cp 172.16.32.42 21,53,80,3306,6379,11211,27017
  - This will check ports again but without -nr (no restart) switch 
5) All services are defined in yaml file (etc/services.yml). If you wanna restart service that is not defined or service on other box, you will need to edit it.
6) To list available services support use
  ./checker.rb ls
7) To modify/add new services, use
  ./checker.rb ams PORT IP(s) NAME SERVICE
8) Try scanning some ral world networks
  ./checker.rb cp 8.8.8.0/24 53 -nr -t0.5
  - This will check port 53 on selected C class with connection timeout of 0.5 seconds
9) Try to add service or enable existing for some of your IPs and then try to restart it through script
  ./checker cp 192.168.0.66 1-1024 -t0.1 -ublockout
  - This will try to restart all configured services with port between 1 and 1024, that are enabled for IP 192.168.0.66, with timeout of 0.1s and through ssh with username blockout. User should have authorized keys and sudo enabled for this to work
10) All logs are in log/checker.log

