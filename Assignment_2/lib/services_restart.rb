require 'yaml'
require 'open3'

module Restart
  
  class Services  < Struct.new(:name, :service, :ips)

    module All
      attr_accessor :all
    end
    
    extend All
    
    
    def all
      self.class.all
    end


    def self.load
      
      return self.all if !self.all.nil? && !self.all.empty?

      data_file = "./etc/services.yml"

      self.all = {}
      YAML.load(File.read(data_file)).each_pair do |key, c|
        self.all[key] = Services.new(c[:name], c[:service], c[:ips])
      end
      self.all
      
    end
    
    
    def self.find_by_port(port)
      self.all[port]
    end
    
    def self.find_by_ip(ip)
      if enabled = self.all.each.select{|x| x[1].ips.include? ip}
        enabled
      end
    end
    
    def self.find_by_name(name)
      if enabled = self.all.detect{|x| x[1].name == name}
        enabled
      end
    end
    
    def self.now(ip, service, username=nil)
      
      if username
        command = "ssh -t -t #{username}@#{ip} \"sudo service #{service} start\""
      else
        command = "vagrant ssh #{ip} -c \"sudo service #{service} start\" -- -t -t "
      end
      
      out = ""
      error = ""
      
      Open3.popen3(command) do |i, o, e, th|

        t_err = Thread.new {
          while !e.eof?  do
            error += e.readchar
          end
        }

        t_out = Thread.new {
          while !o.eof?  do
            out += o.readchar
          end
        }

        Process::waitpid(th.pid) rescue nil 
        
        t_err.join
        t_out.join
        
        Util.log "checker.log", "STDOUT: #{out.strip!}"
        Util.log "checker.log", "STDERR: #{error.strip!}", "error"

      end
      
    end
    
    
  end
  
end