require 'logger'

module Util

  def self.log( fname, line, type="info" )
    
    line = line.join("\t") if line.class.name == "Array" 
    line = "[User: #{USER}] -- " + line
    
    logs = Logger.new("./log/#{fname}")
    
    if type == "error"
      logs.error line
    elsif type == "warn"
      logs.warn line
    else
      logs.info line
    end
  
  end

end