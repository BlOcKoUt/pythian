require 'socket'
require 'timeout'
require 'ipaddr'

module Checker

  def self.valid_ip?(ip)
    begin
      all_ips = []
      check_ip = IPAddr.new(ip)
      check_ip.to_range.each{|x| all_ips << x.to_s}
      all_ips
    rescue
      false
    end
  end

  def self.valid_port?(port)
    all_ports = []
    if port.match(/^\d{1,5}$/)
      port.to_i > 65535 ? all_ports = nil : all_ports << port.to_i
    elsif port.match(/^\d{1,5}\..\d{1,5}$/)
      ports = port.split("..")
      ports.first.upto(ports.last) do |x|
        x.to_i > 65535 ? all_ports = nil : all_ports << x.to_i 
      end
    elsif port.match(/^\d{1,5}\-\d{1,5}$/)
      ports = port.split("-")
      ports.first.upto(ports.last) do |x|
        x.to_i > 65535 ? all_ports = nil : all_ports << x.to_i 
      end
    elsif port.include? ","
      ports = port.split(",")
      ports.each do |x|
        if x.match(/^\d{1,5}$/) && x.to_i < 65535
          all_ports << x.to_i
        else
          all_ports = nil
          break
        end
      end
    else
      all_ports = nil
    end
  
    all_ports ? all_ports : false
  end

  def self.port_open?(ip, port, seconds=1)
    Timeout::timeout(seconds) do
      begin
        TCPSocket.new(ip, port).close
        true
      rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH, Errno::EACCES, Errno::EHOSTDOWN, Errno::ENETUNREACH
        false
      end
    end
  rescue Timeout::Error
    false
  end

end