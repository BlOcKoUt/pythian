Assigment 1:

- Using a vagrantfile which executes any configuration management framework (e.g. chef, puppet, ansible or salt) or docker, automatically stand up a data driven website (of any stack) from scratch.


Solution:

- This is just an proof of concep. This vagrantfile will bring up two boxes, first for db and second for WordPress. Puppet is used for configuration management. I have used hostonly and final website will be on http://172.16.32.66.
