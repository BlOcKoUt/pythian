#
# Performs initial configuration tasks for all Vagrant boxes.
#

class baseconfig {
  exec { 'apt-get update':
    command => '/usr/bin/apt-get update';
  }

  host {
    'hostmachine':
      ip => '172.16.32.1';

    'db':
      ip => '172.16.32.65';

    'web':
      ip => '172.16.32.66';

  }

  file {
    '/home/vagrant/.bashrc':
      owner => 'vagrant',
      group => 'vagrant',
      mode  => '0644',
      source => 'puppet:///modules/baseconfig/bashrc';
  }
  package { 'mysql-client': ensure => installed,
    require => Exec['apt-get update'];
  }

}
