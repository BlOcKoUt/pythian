Assigment 3:

- Create a vagrant box that runs hadoop and monitoring tools watching it.

Solution:

- For this one I am using 3 box Hortonworks and Ambari for configuration/monitoring. Those 3 boxes are set to 1Gb RAM each, but for better performance set this to 2Gb. Unfortunatelly, there are still some parts 
that need to be done manually, and hopefully I will fix that too. Boxes are named One, Two and Three. Ambari server is on machine "one" and all three machines run Ambari agents. When boxes are up, here are steps
that need to be done manually:

1) Log into Ambari graphical interface to setup and configure cluster. It is on http://172.16.32.252:8080/ and credentials are admin/admin
2) On welcome screen, name your cluster (e.g. Pythian)
3) Select Stack (chose default HDP 2.1)
4) On 3rd screen, in Target Hosts put one.cluster, two.cluster and three.cluster. Select "Perform manual registration on hosts and do not use SSH" and click Register and Confirm.
It will warn you that ambari agents should be installed on each host, but don't worry, they are already there.
5) All three hosts should pass test, so just click Next when test is done.
6) Now is time to choose some services that we wanna run. Feel free to run all of them ;)
7) Chose machines on which services will run. Feel free to use default.
8) Now assign slaves and clients. Default one is also good option for this presentation.
9) Time to customize services (enter some missing passwords). Check red signs on the top and feel free to use pythian as password everywhere. Also, you will need to set Nagios admin email. 
10) Finally, review what have we done, and click Deploy. This will take a while, so be patient ;)

Thats it, we should now have a complete Hadoop cluster ;)
